const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");


router.post("/checkEmail", (req, res) =>{
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Router for the user registration
router.post("/register",(req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for the user login (with token creation)
router.post("/login", (req, res)=>{
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for the retrieving the current user's details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	console.log(userData);

	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

router.patch("/:userId", auth.verify, (req, res)=>{
const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){			
	userControllers.updateUserStatus(req.params.userId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission on this page!")
	}

});




module.exports = router;
