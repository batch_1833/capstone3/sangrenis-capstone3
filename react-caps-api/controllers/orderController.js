const Order = require('../models/Order');
const Cart = require('../models/Cart');
const cartController = require('../controllers/cartController')
const mongoose = require('mongoose');


module.exports.getAllOrders = async (payload) => {

	if(payload.isAdmin === true) {
		return Order.find({}).then(result => {
			return (result);
		})
	}
	else {
		return 'Unauthorized Access.'
	}
}

module.exports.getTotalRevenue = async (payload) => {
	if(payload.isAdmin === true) {
		return Order.aggregate([
			{ $match: { bill: {$gte: 0} } },
    		{ $group: { _id: null, totalRevenue: { $sum: "$bill" } } }
    		]).then(result => {
				return result;
		})
	}
	else {
		return 'Unauthorized access';
	}
}


module.exports.getOrderHistory = (payload) => {
	return Order.find({userId: payload.id}).then(result => {
		return result;
	})
}

module.exports.checkout = (payload) => {
	return Cart.findOne({userId: payload.id}).then(result => {
		if(result) {
			const {userId, products, bill} = result;

			let order = new Order({
				userId: userId,
				products: products,
				bill: bill
			});

			return order.save().then((order, err) => {
				if(err) {
					return false;
				}
				else {
					console.log(order.userId);
					cartController.deleteCart(order.userId);
					return order;
				}
			})
		}
		else {
			return false;
		}
	})
}