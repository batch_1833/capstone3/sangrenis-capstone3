const jwt = require("jsonwebtoken");
const secret =  "cApst0ne2";

module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

// Middleware functions
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	console.log(typeof token);

	if(typeof token !== "undefined"){
		console.log(token);
		// This removes the "Bearer " prefix and obtains only the token for verification.
		token = token.slice(7, token.length);
		console.log(token);

		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{

				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}
}

module.exports.decode = (token) =>{

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return null;
			}
			else{
				// Syntax: jwt.decode(token, [options])
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}

}
