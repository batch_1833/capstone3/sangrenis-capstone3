import Banner from "../components/Banner";
import CarouselSec from "../components/Carousel";
import FeaturedSec from '../components/FeaturedSec';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';

export default function Home(){
	
	return(
		<Fragment>
			<CarouselSec/>
			<Container>
				<Banner/>
				<FeaturedSec/>
			</Container>
		</Fragment>
	)
}