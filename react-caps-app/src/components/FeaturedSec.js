
import { Row, Col, Button } from 'react-bootstrap';


export default function CarouselSection() {
    return (
        <div className="my-5">
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured1.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Basketball Shoes</h4>
                        <p className="w-lg-75 gray">
                         is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear
                        </p>
                        <Button

                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Shop Now
                        </Button>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={7} className="p-0 order-lg-2">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured2.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3 order-lg-1">
                        <h4>Hiking Shoes</h4>
                        <p className="w-lg-75 gray">
                            Express your creativity through our adidas originals shoes and footwear, a perfect fusion of sport and style. Buy online today, delivered to your door.
                        </p>
                        <Button
                        id='62ea69aaf972fddfff324c44'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Shop Now
                        </Button>
                    </div>
                </Col>
            </Row>            
            <Row>
                <Col xs={12} md={7} className="p-0">
                    <img
                        className="img-fluid"
                        src={require('./imgFeatured3.jpg')}
                        alt="First slide"
                    />
                </Col>
                <Col xs={12} md={5}>
                    <div className="d-flex flex-column align-items-center justify-content-center p-3 mt-xl-3">
                        <h4>Customized Control</h4>
                        <p className="w-lg-75 gray">
                            It’s not always easy to find a quiet place to take a call. The microphone system built into these earbuds is specially designed to pick up the sound of your voice — while it rejects most of the noise around you.
                        </p>
                        <Button
                        id='62ea69bef972fddfff324c46'
                        buttonStyle='btn--secondary'
                        buttonSize='btn--medium'>
                        Shop Now
                        </Button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}